import unittest
import helltriangle

class HellTriangleTest(unittest.TestCase):

	def test_solvetriangle_emptyarray(self):
		self.assertEqual(helltriangle.solve_triangle([]), 0)

	def test_solvetriangle_one_element_array(self):
		self.assertEqual(helltriangle.solve_triangle([[10]]), 10)

	def test_solvetriangle_valid_triangle(self):
		self.assertEqual(helltriangle.solve_triangle([[6],[3,5],[9,7,1],[4,6,8,4]]), 26)

	def test_solvetriangle_malformed_triangle(self):
		with self.assertRaises(helltriangle.MalformedTriangleException):
			helltriangle.solve_triangle([[10],[-1]])

	def test_findmax_empty_array(self):
		self.assertEqual((None, None), helltriangle.find_max([], -1))

	def test_findmax_one_element_array(self):
		self.assertEqual((1, 0), helltriangle.find_max([1], -1))

	def test_findmax_nearest_greater_than_array_size(self):
		self.assertEqual((None, None), helltriangle.find_max([1,2], 5))

	def test_findmax_nearest_valid(self):
		self.assertEqual((0,0), helltriangle.find_max([0,-1], 0))
		self.assertEqual((1,1), helltriangle.find_max([0,1], 0))
		self.assertEqual((1,0), helltriangle.find_max([1,0], 0))


if __name__ == '__main__':
	unittest.main()