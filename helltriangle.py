class MalformedTriangleException(Exception):
	def __init__(self, message):
		self.message = message
	def __str__(self):
		return self.message

def find_max(array, nearest):
	array_size = len(array)
	if array_size == 0:
		return (None, None)
	elif array_size == 1:
		return (array[0], 0)
	elif nearest > array_size:
		return (None, None)
	if array[nearest] > array[nearest+1]:
		return (array[nearest], nearest)
	return (array[nearest+1], nearest+1)

def solve_triangle(array):
	acc = []
	nearest = -1
	array_size = len(array)
	while array_size > 0:
		if len(array[0]) <= nearest+1: raise MalformedTriangleException("Malformed triangle, array size should be: %d" % (array_size+1)) 
		value, nearest = find_max(array[0], nearest)
		acc.append(value)
		array.pop(0)
		array_size = len(array)
	return sum(acc)

if __name__ == '__main__':
	import sys, ast
	# Reference: http://stackoverflow.com/questions/1450393/how-do-you-read-from-stdin-in-python
	f = open(sys.argv[1]) if len(sys.argv) > 1 else sys.stdin
	for line in f:
		array = ast.literal_eval(line)
		print solve_triangle(array)
	f.close()